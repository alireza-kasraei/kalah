package net.devk.kalah;

public enum GameStatus {
	PENDING, PLAYER_ONE_WON, PLAYER_TWO_WON, EQUAL;
}