package net.devk.kalah;

public class IllegalPlayException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public IllegalPlayException(String message) {
		super(message);
	}

}
